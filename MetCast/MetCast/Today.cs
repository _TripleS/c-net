﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConsoleApplication4;


namespace MetCast
{
	public partial class Today : Form
	{
		Weather weather;
		public Today(Weather _weather)
		{
			InitializeComponent();
			weather = _weather;
			string result = "";
			result += weather.Date.ToShortDateString() + " " + weather.DayOfWeek + '\n';
			result += weather.MaxTemeperature + '\n';
			result += weather.MinTemperature + '\n';
			result += weather.Sci + '\n';
			label3.Text = result;
			pictureBox1.Image = DrawPicture(); 

		}

		private Image DrawPicture()
		{
			Image image;
			try
			{
				image = Image.FromFile(weather.Sci + ".jpg");
			}
			catch
			{
				image = Image.FromFile("Error.jpg");
			}
			return image;
		}
	}
}
