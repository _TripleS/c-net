﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4
{
	public class Weather
	{

		public Weather(string _dayOfWeek, DateTime _date, string _minT, string _maxT, string _sci)
		{
			DayOfWeek = _dayOfWeek;
			Date = _date;
			MinTemperature = _minT;
			MaxTemeperature = _maxT;
			Sci = _sci;
		}
		public string DayOfWeek { get; private set; }

		public DateTime Date { get; private set; }

		public string MinTemperature { get; private set; }

		public string MaxTemeperature { get; private set; }

		public string Sci { get; private set; }
		
	}
}
