﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConsoleApplication4;

namespace MetCast
{
	public partial class Week : Form
	{
   
      
        public Week(Weather weather0, Weather weather1, Weather weather2, Weather weather3, Weather weather4, Weather weather5, Weather weather6)
		{
			InitializeComponent();
          

            string result0 = "";
            string result1 = "";
            string result2 = "";
            string result3 = "";
            string result4 = "";
            string result5 = "";
            string result6 = "";

            result0 += weather0.Date.ToShortDateString() + " " + weather0.DayOfWeek + '\n';
            result0 += weather0.MaxTemeperature + '\n';
            result0 += weather0.MinTemperature + '\n';
            result0 += weather0.Sci + '\n';

            result1 += weather1.Date.ToShortDateString() + " " + weather1.DayOfWeek + '\n';
            result1 += weather1.MaxTemeperature + '\n';
            result1 += weather1.MinTemperature + '\n';
            result1 += weather1.Sci + '\n';

            result2 += weather2.Date.ToShortDateString() + " " + weather2.DayOfWeek + '\n';
            result2 += weather2.MaxTemeperature + '\n';
            result2 += weather2.MinTemperature + '\n';
            result2 += weather2.Sci + '\n';

            result3 += weather3.Date.ToShortDateString() + " " + weather3.DayOfWeek + '\n';
            result3 += weather3.MaxTemeperature + '\n';
            result3 += weather3.MinTemperature + '\n';
            result3 += weather3.Sci + '\n';

            result4 += weather4.Date.ToShortDateString() + " " + weather4.DayOfWeek + '\n';
            result4 += weather4.MaxTemeperature + '\n';
            result4 += weather4.MinTemperature + '\n';
            result4 += weather4.Sci + '\n';

            result5 += weather5.Date.ToShortDateString() + " " + weather5.DayOfWeek + '\n';
            result5 += weather5.MaxTemeperature + '\n';
            result5 += weather5.MinTemperature + '\n';
            result5 += weather5.Sci + '\n';

            result6 += weather6.Date.ToShortDateString() + " " + weather6.DayOfWeek + '\n';
            result6 += weather6.MaxTemeperature + '\n';
            result6 += weather6.MinTemperature + '\n';
            result6 += weather6.Sci + '\n';

            label1.Text = result0;
            label2.Text = result1;
            label3.Text = result2;
            label4.Text = result3;
            label5.Text = result4;
            label6.Text = result5;
            label7.Text = result6;

            pictureBox1.Image = DrawPicture(weather0);
            pictureBox2.Image = DrawPicture(weather1);
            pictureBox3.Image = DrawPicture(weather2);
            pictureBox4.Image = DrawPicture(weather3);
            pictureBox5.Image = DrawPicture(weather4);
            pictureBox6.Image = DrawPicture(weather5);
            pictureBox7.Image = DrawPicture(weather6); 
		}


        private Image DrawPicture(Weather _weather)
        {
			Image image;
			try
			{
				image = Image.FromFile(_weather.Sci + ".gif");
			}
			catch
			{
				image = Image.FromFile("Error.jpg");
			}
			return image;
        }

     
	}
}
