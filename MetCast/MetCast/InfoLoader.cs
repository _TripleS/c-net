﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApplication4;
using HtmlAgilityPack;

namespace MetCast
{
	class InfoLoader
	{
		List<Weather> weather;

		public void FindWeather(string city)
		{
			string webSite = @"https://sinoptik.ua/погода-";

			webSite += city;

			HtmlAgilityPack.HtmlDocument document = LoadWebSite(webSite);
			weather = ParseHtmlTo(document);
		}

		public HtmlAgilityPack.HtmlDocument LoadWebSite(string _adress)
		{
			System.Net.WebClient web = new System.Net.WebClient();
			web.Encoding = UTF8Encoding.UTF8;

			HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();

			try
			{
				string str = web.DownloadString(_adress);

				document.LoadHtml(str);
			}
			catch
			{
				document = null;
			}
			return document;
		}

		public List<Weather> ParseHtmlTo(HtmlAgilityPack.HtmlDocument document)
		{
			if (document == null)
				return null;
			string[] result = null;
			List<string> sci = new List<string>();
			List<Weather> weather = new List<Weather>();
			var divList = document.DocumentNode.SelectNodes("//div");

			foreach (var div in divList)
			{
				var attributes = div.Attributes;
				foreach (var att in attributes)
				{
					if (att.Name == "class" && att.Value == "tabs")
					{
						string stringRes = att.OwnerNode.InnerText;
						stringRes = stringRes.Replace("&nbsp", "");
						stringRes = stringRes.Replace("&deg", "");
						stringRes = stringRes.Replace("\t", "");
						stringRes = stringRes.Replace(" ", "");
						stringRes = stringRes.Replace(";", "");
						result = stringRes.Split('\n');

					}
					else if (att.Name == "class" && att.Value.Contains("weatherIco"))
					{
						var qwer = att.OwnerNode.Attributes;
						foreach (var asd in qwer)
						{
							if (asd.Name == "title")
								sci.Add(asd.Value);
						}
					}
				}
			}

			for (int day = 3, number = 4, month = 5, minT = 8, maxT = 9, heaven = 0;
				 day < result.Count();
				 day += 11, number += 11, month += 11, minT += 11, maxT += 11, heaven++)
			{
				int dateDay;
				int dateMonth = GetMonth(result[month]);
				int.TryParse(result[number], out dateDay);
				DateTime date = new DateTime(DateTime.Today.Year, dateMonth, dateDay);
				weather.Add(new Weather(result[day], date, result[minT], result[maxT], sci[heaven]));
			}
			return weather;
		}

		private int GetMonth(string _month)
		{
			string[] months = { "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря" };
			_month.Replace(" ", "");
			for (int i = 0; i < months.Count(); i++)
				if (months[i] == _month)
					return i + 1;
			throw new Exception("Invalid data");
		}

		public bool IsLoaded()
		{
			return weather != null;
		}

		public void ShowWeatherToWeek()
		{
			Week form2 = new Week(weather[0], weather[1], weather[2], weather[3], weather[4], weather[5], weather[6]);
			form2.ShowDialog();
		}

		public void ShowWeatherAtToday()
		{
			Today form2 = new Today(weather[0]);
			form2.ShowDialog();
		}
	}
}
