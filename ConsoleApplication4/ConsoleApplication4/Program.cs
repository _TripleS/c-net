﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using HtmlAgilityPack;

namespace ConsoleApplication4
{
	class Program
	{
		static void Main(string[] args)
		{
			string webSite = @"https://sinoptik.ua/погода-харьков";

			Console.WriteLine("Введите название города:");
			string cityName = Console.ReadLine();

			webSite = webSite.Replace("харьков", cityName);

			HtmlDocument document = LoadWebSite(webSite);
			List<Weather> weather = ParseHtmlTo(document);

			PrintWeatherToWeek(weather);

			Console.Read();
		}

		public static HtmlDocument LoadWebSite(string _adress)
		{
			System.Net.WebClient web = new System.Net.WebClient();
			web.Encoding = UTF8Encoding.UTF8;
			
			HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();

			string str = web.DownloadString(_adress);
			document.LoadHtml(str);
			return document;
		}

		public static List<Weather> ParseHtmlTo(HtmlDocument document)
		{

			string[] result = null;
			string cityInfo = null;
			List<string> sci = new List<string>();
			List<Weather> weather = new List<Weather>();			

			var divList = document.DocumentNode.SelectNodes("//div");

			foreach (var div in divList)
			{
				var attributes = div.Attributes;
				foreach (var att in attributes)
				{
					if (att.Name == "class" && att.Value == "cityName cityNameShort")
					{
						cityInfo = att.OwnerNode.InnerText;
					}
					else if (att.Name == "class" && att.Value == "tabs")
					{
						string stringRes = att.OwnerNode.InnerText;
						stringRes = stringRes.Replace("&nbsp", "");
						stringRes = stringRes.Replace("&deg", "");
						stringRes = stringRes.Replace("\t", "");
						stringRes = stringRes.Replace(" ", "");
						stringRes = stringRes.Replace(";", "");
						result = stringRes.Split('\n');

					}
					else if (att.Name == "class" && att.Value.Contains("weatherIco"))
					{
						var qwer = att.OwnerNode.Attributes;
						foreach (var asd in qwer)
						{
							if (asd.Name == "title")
								sci.Add(asd.Value);
						}

					}
				}
			}

			for (int day = 3, number = 4, month = 5, minT = 8, maxT = 9, heaven = 0;
				 day < result.Count();
				 day += 11, number += 11, month += 11, minT += 11, maxT += 11, heaven++)
			{
				weather.Add(new Weather(cityInfo, result[day], result[number] + " " + result[month], result[minT], result[maxT], sci[heaven]));
			}
			return weather;
		}

		public static void PrintWeatherAt(string _date, List<Weather> _weather)
		{
			foreach (var w in _weather)
			{
				if (w.Date == _date)
				{
					Console.WriteLine(w.CityName);
					Console.WriteLine(w.Date + " " + w.DayOfWeek);
					Console.WriteLine(w.MaxTemeperature);
					Console.WriteLine(w.MinTemperature);
					Console.WriteLine(w.Sci);
					return;
				}
				Console.WriteLine("Date not found sorry");
			}
		}

		public static void PrintWeatherToWeek(List<Weather> _weather)
		{
			Console.WriteLine(_weather[0].CityName);
			foreach (var w in _weather)
			{
				Console.WriteLine(w.Date + " " + w.DayOfWeek);
				Console.WriteLine(w.MaxTemeperature);
				Console.WriteLine(w.MinTemperature);
				Console.WriteLine(w.Sci);
				Console.WriteLine();
			}
		}

	}
}
