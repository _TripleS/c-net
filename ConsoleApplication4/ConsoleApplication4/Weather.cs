﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4
{
	class Weather
	{
		string cityName;
		string dayOfWeek;
		string date;
		string minTemperature;
		string maxTemperature;
		string sci;

		public Weather(string _cityName, string _dayOfWeek, string _date, string _minT, string _maxT, string _sci)
		{
			cityName = _cityName;
			dayOfWeek = _dayOfWeek;
			date = _date;
			minTemperature = _minT;
			maxTemperature = _maxT;
			sci = _sci;
		}
		public string DayOfWeek
		{
			get
			{
				return dayOfWeek;
			}
		}

		public string Date
		{
			get
			{
				return date;
			}
		}

		public string MinTemperature
		{
			get
			{
				return minTemperature;
			}
		}

		public string MaxTemeperature
		{
			get
			{
				return maxTemperature;
			}
		}

		public string Sci
		{
			get
			{
				return sci;
			}
		}

		public string CityName
		{
			get
			{
				return cityName;
			}
		}

	}
}
